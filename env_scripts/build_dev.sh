NOVA_AUTH_URL=https://novaauthsso.webfact.corproot.net
PROXY=proxy.vptt.ch
echo "Auth URL was set to $NOVA_AUTH_URL"
echo "Proxy was set to $PROXY"

#Drush commands
drush vset http_proxy $PROXY
drush -y vdel update_notify_emails
drush -y dl prod_check
drush -y en prod_check
drush -y prod-check-prodmode
drush vset prod_check_xmlrpc_key aKEY4dock2
drush vset prod_check_enable_xmlrpc 1
